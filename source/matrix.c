#include <stdarg.h>
#include <stdio.h>
#include "matrix.h"

struct matrix {
    int columns;
    int rows;
    double **values;
};

matrix newMatrix(int columns, int rows) {

    if (columns <= 0 || rows <= 0) return NULL;

    matrix m = (matrix) malloc(sizeof(matrix));

    m->columns  = columns;
    m->rows     = rows;

    m->values = (double **) calloc(columns, sizeof(double*));

    int c, r;

    for (c = DEFAULT_START_POSITION; c <= columns; c++) {
        COLUMN(m, c) = (double *) calloc(rows, sizeof(double));
        for (r = DEFAULT_START_POSITION; r <= rows; r++){
            ELEMENT(m, c, r) = DEFAULT_MATRIX_VALUE;
        }
    }

    return m;
}

void setElement(matrix m, int column, int row, double value) {
    ELEMENT(m, column, row) = value;
}

double getElement(matrix m, int column, int row) {
    return ELEMENT(m, column, row);
}

matrix copyMatrix(matrix m) {
    if (m == NULL) return NULL;

    matrix mAux = (matrix) malloc(sizeof(matrix));

    mAux->columns   = m->columns;
    mAux->rows      = m->rows;

    mAux->values = (double **) calloc(mAux->columns, sizeof(double*));

    int c, r;
    for (c = DEFAULT_START_POSITION; c <= mAux->columns; c++) {
        COLUMN(mAux, c) = (double *) calloc(mAux->rows, sizeof(double));
        for (r = DEFAULT_START_POSITION; r <= mAux->rows; r++){
            ELEMENT(mAux, c, r) = ELEMENT(m, c, r);
        }
    }

    return mAux;
}

matrix sumMatrixes(matrix m1, matrix m2) {
    if (
        m1 == NULL || 
        m2 == NULL || 
        m1->columns != m2->columns || 
        m1->rows != m2->rows
        ) return NULL;
    
    matrix m = newMatrix(m1->columns, m1->rows);

    int c, r;
    for ( c = DEFAULT_START_POSITION; c <= m->columns; c++){
        for ( r = DEFAULT_START_POSITION; r <= m->rows; r++){
            ELEMENT(m, c, r) = ELEMENT(m1, c, r) + ELEMENT(m2, c, r);
        }
    }
    
    return m;
}

matrix sumMatrix(matrix m1, double value) {
    if (
        m1 == NULL
        ) return NULL;

    matrix m = copyMatrix(m1);

    int c, r;
    for ( c = DEFAULT_START_POSITION; c <= m->columns; c++) {
        for ( r = DEFAULT_START_POSITION; r <= m->rows; r++) {
            ELEMENT(m, c, r) = ELEMENT(m1, c, r) + value;
        }
    }
    
    return m;
}

matrix subtractMatrixes(matrix m1, matrix m2) {
    if (
        m1 == NULL || 
        m2 == NULL || 
        m1->columns != m2->columns || 
        m1->rows != m2->rows
        ) return NULL;
    
    matrix m = newMatrix(m1->columns, m1->rows);

    int c, r;
    for ( c = DEFAULT_START_POSITION; c <= m->columns; c++){
        for ( r = DEFAULT_START_POSITION; r <= m->rows; r++){
            ELEMENT(m, c, r) = ELEMENT(m1, c, r) - ELEMENT(m2, c, r);
        }
    }
    
    return m;
}

matrix subtractMatrix(matrix m1, double value) {
    if (
        m1 == NULL
        ) return NULL;

    matrix m = copyMatrix(m1);

    int c, r;
    for ( c = DEFAULT_START_POSITION; c <= m->columns; c++) {
        for ( r = DEFAULT_START_POSITION; r <= m->rows; r++) {
            ELEMENT(m, c, r) = ELEMENT(m1, c, r) - value;
        }
    }
    
    return m;
}

matrix multiplyMatrixes(matrix m1, matrix m2) {
    if (
        m1 == NULL || 
        m2 == NULL || 
        m1->columns != m2->rows
        ) return NULL;
    
    matrix m = newMatrix(m2->columns, m1->rows);

    int c, r, i;
    for ( c = DEFAULT_START_POSITION; c <= m->columns; c++){
        for ( r = DEFAULT_START_POSITION; r <= m->rows; r++){
            for ( i = DEFAULT_START_POSITION; i <= m1->columns; i++){
                ELEMENT(m, c, r) = ELEMENT(m, c, r) + ELEMENT(m1, i, r) * ELEMENT(m2, c, i);
            }
        }
    }
    
    return m;
}


matrix multiplyMatrix(matrix m1, double value) {
    if (
        m1 == NULL
        ) return NULL;

    matrix m = copyMatrix(m1);

    int c, r;
    for ( c = DEFAULT_START_POSITION; c <= m1->columns; c++) {
        for ( r = DEFAULT_START_POSITION; r <= m1->rows; r++) {
            ELEMENT(m, c, r) = ELEMENT(m1, c, r) * value;
        }
    }

    return m;
}

matrix transposeMatrix(matrix m1) {
    if (
        m1 == NULL
        ) return NULL;

    matrix m = newMatrix(m1->columns, m1->rows);

    int c, r;
    for ( c = DEFAULT_START_POSITION; c <= m->columns; c++) {
        for ( r = DEFAULT_START_POSITION; r <= m->rows; r++) {
            ELEMENT(m, c, r) = ELEMENT(m1, r, c);
        }
    }

    return m;
}

matrix getCofactor(matrix m1, int p, int q){
    int i, j;
    int c, r;
    matrix m = newMatrix(m1->columns-1, m1->rows-1);
    
    for (c = DEFAULT_START_POSITION, j = 0; c <= m1->columns; c++){
        if (c==p) continue;
        j++;
        for (r = DEFAULT_START_POSITION, i = 0; r <= m1->rows; r++){
            if (r==q) continue;
            i++;
                
            ELEMENT(m, j, i) = ELEMENT(m1, c, r);
        }
    }

    return m;
}

double determinantMatrix(matrix m1) {
    if (isSquare(m1) == 0) return 0;

    int d = 0;

    if (m1->columns == 1) return ELEMENT(m1, 1, 1);

    matrix m;
    double sign;

    int i=1;
    for ( i = DEFAULT_START_POSITION; i <= m1->columns; i++) {
        sign = (i) % 2 == 0 ? -1.0 : 1.0; 
        m = getCofactor(m1, i, 1);
        d += sign * ELEMENT(m1, i, 1) * determinantMatrix(m);
        freeMatrix(m);
    }

    return d;
}

matrix adjMatrix(matrix m1) {
    if (isSquare == 0) return NULL;

    matrix m = newMatrix(m1->columns, m1->rows);
    
    int i, j;
    double sign;
    for (j=1; j<=m->columns; j++){
        for (i=1; i<=m->rows; i++) {
            matrix mAux = getCofactor(m1, j, i);
            sign = (j+i) % 2 == 0 ? 1.0 : -1.0; 
            ELEMENT(m, j, i) = sign * determinantMatrix(mAux);
        }
    }

    matrix mTranspose = transposeMatrix(m);
    freeMatrix(m);

    return mTranspose;
}

matrix inverseMatrix(matrix m1) {
    if (isSquare == 0) return NULL;
    
    double detM1 = determinantMatrix(m1);
    if (detM1 == 0.0) return NULL;

    matrix mAdj = adjMatrix(m1);
    matrix mInverse = multiplyMatrix(mAdj, (1.0/detM1));

    freeMatrix(mAdj);
    return mInverse;
}


int isSquare(matrix m1){
    return m1 != NULL && m1->columns == m1->rows;
}

void freeMatrix(matrix m) {
    if (m == NULL) return;

    int c;
    for (c = 0; c < m->columns; c++) {
        free(m->values[c]);
    }

    free(m->values);
    free(m);
}

void printMatrix(matrix m) {
    if (m == NULL) {
        printf("\nMatrix empty.");
        return;
    }

    printf("\nMatrix: %dx%d", m->columns, m->rows);

    int c, r;
    for (r = DEFAULT_START_POSITION; r <= m->rows; r++){
        printf("\n");
        for (c = DEFAULT_START_POSITION; c <= m->columns; c++) {
            printf("\t%0.2lf",ELEMENT(m,c,r));
        }
    }
}
