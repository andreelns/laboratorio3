#include <stdio.h>
#include "matrix.h"

int main(){
    printf("Teste de matrix\n\n");

    matrix ma = newMatrix(2,2);
    matrix mb = newMatrix(2,2);

    matrix mRandom = newMatrix(5,5);
    int i, j;
    for (i = 1; i <= 5; i++){
        for (j = 1; j <= 5; j++){
            setElement(mRandom,j,i, rand() % 10);
        }
    }
    
    setElement(ma,1,1,1);
    setElement(ma,1,2,3);
    setElement(ma,2,1,2);
    setElement(ma,2,2,4);

    setElement(mb,1,1,5);
    setElement(mb,1,2,7);
    setElement(mb,2,1,6);
    setElement(mb,2,2,8);


    matrix mCopy = copyMatrix(ma);
    matrix mS5 = sumMatrix(mCopy, 5);
    matrix mM10 = multiplyMatrix(mCopy, 10);
    matrix mS = sumMatrixes(ma, mb);
    matrix mM = multiplyMatrixes(ma, mb);
    matrix mTranspose = transposeMatrix(mRandom);
    matrix mInverse = inverseMatrix(mRandom);

    printf("\n\nPrint Matrixes\n");
    printf("\nma: ");
    printMatrix(ma);
    printf("\nmb: ");
    printMatrix(mb);
    printf("\nmCopy copied ma: ");
    printMatrix(mCopy);
    printf("\nmS5 sum escalar 5: ");
    printMatrix(mS5);
    printf("\nmS sum ma mb: ");
    printMatrix(mS);
    printf("\nmM10 mult escalar 10: ");
    printMatrix(mM10);
    printf("\nmM mult ma mb: ");
    printMatrix(mM);
    printf("\nmRandom: ");
    printMatrix(mRandom);
    printf("\nmTranspose: transpose of mRandom");
    printMatrix(mTranspose);
    printf("\nmInverse: inverse of mRandom");
    printMatrix(mInverse);

    printf("\n\nPrint determinants\n");
    double detMA = determinantMatrix(ma);
    printf("\ndetMA: %0.4lf", detMA);
    double detMB = determinantMatrix(mb);
    printf("\ndetMB: %0.4lf", detMB);
    double detM = determinantMatrix(mM);
    printf("\ndetM: %0.4lf", detM);
    double detRandom = determinantMatrix(mRandom);
    printf("\ndetRandom: %0.4lf", detRandom);

    printf("\n");

    freeMatrix(ma);
    freeMatrix(mb);
    freeMatrix(mCopy);
    freeMatrix(mM);
    freeMatrix(mM10);
    freeMatrix(mS);
    freeMatrix(mS5);
    freeMatrix(mRandom);
    freeMatrix(mInverse);
    freeMatrix(mTranspose);
    return 0;
}
