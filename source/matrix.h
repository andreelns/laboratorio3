#ifndef MATRIX_H
#define MATRIX_H
#include <stdlib.h>

#define DEFAULT_MATRIX_VALUE 0.0
#define DEFAULT_START_POSITION 1

#define ELEMENT(m, c, r) m->values[c-1][r-1]
#define COLUMN(m, c) m->values[c-1]

struct matrix;
typedef struct matrix* matrix;

matrix newMatrix(int column, int rows);
matrix copyMatrix(matrix m);

void setElement(matrix m, int column, int row, double value);
double getElement(matrix m, int column, int row);

matrix sumMatrixes(matrix m1, matrix m2);
matrix sumMatrix(matrix m1, double value);
matrix subtractMatrixes(matrix m1, matrix m2);
matrix subtractMatrix(matrix m1, double value);
matrix multiplyMatrixes(matrix m1, matrix m2);
matrix multiplyMatrix(matrix m1, double value);

matrix transposeMatrix(matrix m1);
matrix adjMatrix(matrix m1);
matrix inverseMatrix(matrix m1);
double determinantMatrix(matrix m1);

int isSquare(matrix m1);

void freeMatrix(matrix m);
void printMatrix(matrix m);

#endif
